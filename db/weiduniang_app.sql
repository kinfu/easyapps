-- phpMyAdmin SQL Dump
-- version 4.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-08-04 12:49:07
-- 服务器版本： 5.5.27
-- PHP Version: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `weiduniang_app`
--

-- --------------------------------------------------------

--
-- 表的结构 `app_app_info`
--

CREATE TABLE IF NOT EXISTS `app_app_info` (
  `title` varchar(128) NOT NULL,
  `name` varchar(64) NOT NULL,
  `intro` varchar(500) NOT NULL,
  `desc` varchar(800) NOT NULL,
  `tpl` varchar(16) DEFAULT 'default',
  `icon` varchar(128) NOT NULL,
`id` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL COMMENT '关联的用户',
  `def_pic` varchar(128) NOT NULL COMMENT '默认图片',
  `logo` varchar(128) NOT NULL,
  `tel` varchar(32) NOT NULL,
  `addr` varchar(128) NOT NULL,
  `contact` varchar(64) NOT NULL,
  `srect` varchar(16) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `app_app_info`
--

INSERT INTO `app_app_info` (`title`, `name`, `intro`, `desc`, `tpl`, `icon`, `id`, `mid`, `def_pic`, `logo`, `tel`, `addr`, `contact`, `srect`) VALUES
('九条笑话', 'jiu tiao xiao hua', '海盐公众文化', '海盐公众文化', 'app1', 'Uploads/Users/icons/app_1_1.png', 1, 1, '', 'http://hywx.weiduzi.com/Public/media/bgpic/26.png', '0573-86022909', '海盐县新桥北路199号', '海盐公众文化', ''),
('', '', '', '', '', '', 2, 2, '', '', '', '', '', ''),
('', '', '', '', '', '', 3, 2, '', '', '', '', '', ''),
('石材报价', '石材报价', '中国石材报价网专业提供高效快捷的石材报价服务，我们立足中国最大的石材加工贸易基地-中国水头，这里有一手的石材价格信息，众多的石材厂家。关注中国石材报价网，提升我们的竞争力！', '石材报价', 'app1', 'Uploads/Users/icons/app_4_1.png', 4, 1, '', 'http://app.weiduniang.com/Public/media/bgpic/17.png', '13777821869', '杭州市', '石材报价', '2ea2be048800edee'),
('测试一个吧', '', '', '', 'default', '', 5, 1, '', '', '', '', '', ''),
('测试一个吧', '', '测试一个吧测试一个吧', '', 'default', '', 6, 1, '', '', '', '', '', ''),
('111', '', '11', '', 'default', '', 7, 1, '', '', '', '', '', ''),
('4454', '', '5454', '', 'default', '', 8, 1, '', '', '', '', '', ''),
('东东', '', '111', '', 'default', '', 9, 1, '', '', '', '', '', ''),
('111111', '', '111111', '', 'default', '', 10, 1, '', '', '', '', '', ''),
('11122211122121', '', '212121212121', '', 'default', '', 11, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `app_info`
--

CREATE TABLE IF NOT EXISTS `app_info` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `intro` text NOT NULL,
  `hits` int(11) NOT NULL,
  `ctime` int(11) NOT NULL,
  `picurl` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `idx` int(11) NOT NULL,
  `navid` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `app_info`
--

INSERT INTO `app_info` (`id`, `mid`, `appid`, `title`, `intro`, `hits`, `ctime`, `picurl`, `url`, `idx`, `navid`) VALUES
(2, 1, 4, '测试信息', '<p>dfdffdsfsdfds</p>', 2, 0, '', '', 0, 14);

-- --------------------------------------------------------

--
-- 表的结构 `app_magic_fileds`
--

CREATE TABLE IF NOT EXISTS `app_magic_fileds` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `type` varchar(12) NOT NULL,
  `tpl` varchar(16) NOT NULL,
  `intro` varchar(600) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `app_magic_list`
--

CREATE TABLE IF NOT EXISTS `app_magic_list` (
`id` int(11) NOT NULL,
  `moid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `intro` varchar(500) NOT NULL,
  `desc` text NOT NULL,
  `picurl` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `idx` int(11) NOT NULL,
  `dtime` int(11) NOT NULL,
  `status` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `app_magic_model`
--

CREATE TABLE IF NOT EXISTS `app_magic_model` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `startime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `num` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `app_magic_order`
--

CREATE TABLE IF NOT EXISTS `app_magic_order` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `moid` int(11) NOT NULL,
  `listid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `dtime` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `app_member`
--

CREATE TABLE IF NOT EXISTS `app_member` (
`id` int(11) NOT NULL,
  `account` varchar(16) NOT NULL,
  `email` varchar(32) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `app_member`
--

INSERT INTO `app_member` (`id`, `account`, `email`, `mobile`, `password`) VALUES
(1, 'admin', 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- 表的结构 `app_nav`
--

CREATE TABLE IF NOT EXISTS `app_nav` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(10) unsigned NOT NULL,
  `widget` varchar(16) NOT NULL,
  `module` varchar(50) NOT NULL DEFAULT '',
  `pk` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '导航名称',
  `intro` varchar(255) NOT NULL,
  `pos` varchar(12) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `icon` varchar(500) NOT NULL DEFAULT '' COMMENT '图标',
  `css` varchar(1000) NOT NULL DEFAULT '' COMMENT '扩展CSS',
  `is_show` tinyint(4) NOT NULL DEFAULT '-1',
  `show_index` tinyint(4) NOT NULL,
  `pid` int(11) NOT NULL,
  `idx` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `app_nav`
--

INSERT INTO `app_nav` (`id`, `mid`, `appid`, `widget`, `module`, `pk`, `name`, `intro`, `pos`, `url`, `icon`, `css`, `is_show`, `show_index`, `pid`, `idx`) VALUES
(16, 1, 4, '', 'Discuz', 97, '高清石材图库', '', 'aside', '', '', '', -1, 1, 0, 0),
(17, 1, 4, '', 'Discuz', 84, '石材养护', '', '-1', '', '', '', -1, 0, 0, 0),
(18, 1, 4, '', 'Discuz', 85, '石材加工', '', 'aside', '', '', '', -1, 1, 0, 0),
(19, 1, 4, '', 'Discuz', 82, '石材机械', '', '-1', '', '', '', -1, 1, 0, 0),
(20, 1, 4, '', 'Discuz', 83, '工具耗材', '', '', '', '', '', -1, 1, 0, 0),
(21, 1, 4, '', 'Discuz', 53, '大理石', '', '1', '', '', '', -1, 1, 0, 0),
(22, 1, 4, '', 'Discuz', 54, '花岗岩', '', '-1', '', '', '', -1, 0, 0, 0),
(24, 1, 4, '', 'Discuz', 55, '玉石', '', '-1', '', '', '', -1, 1, 0, 0),
(25, 1, 4, '', 'Discuz', 92, '重要说明', '', '', '', '', '', -1, 0, 0, 0),
(26, 1, 4, '', 'Discuz', 69, '其它', '', 'aside', '', '', '', -1, 0, 0, 0),
(27, 1, 4, '', 'Discuz', 93, '最新石材资讯', '', '-1', '', '', '', -1, 1, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `app_pages`
--

CREATE TABLE IF NOT EXISTS `app_pages` (
`id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL COMMENT '标题',
  `name` varchar(64) NOT NULL COMMENT '副标题',
  `intro` varchar(600) NOT NULL COMMENT '简介',
  `picurl` varchar(128) NOT NULL COMMENT '封面图片',
  `icon` varchar(128) NOT NULL,
  `appid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `show_top` tinyint(4) NOT NULL COMMENT '顶部菜单',
  `show_bottom` tinyint(4) NOT NULL COMMENT '底部菜单',
  `desc` text NOT NULL COMMENT '内容',
  `show_widget` tinyint(4) NOT NULL COMMENT '在工具中显示',
  `show_left` tinyint(4) NOT NULL COMMENT '在左边显示',
  `show_right` tinyint(4) NOT NULL COMMENT '在右边显示',
  `indx` int(11) NOT NULL COMMENT '排序'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `app_pages`
--

INSERT INTO `app_pages` (`id`, `title`, `name`, `intro`, `picurl`, `icon`, `appid`, `mid`, `show_top`, `show_bottom`, `desc`, `show_widget`, `show_left`, `show_right`, `indx`) VALUES
(1, '福利图', '福利图', '福利图', '', '', 1, 1, 0, 1, '福利图', 0, 1, 0, 0),
(2, '品笑话', '', '品笑话', '', '', 1, 1, 0, 1, '品笑话', 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `app_startpic`
--

CREATE TABLE IF NOT EXISTS `app_startpic` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `picurl` varchar(128) NOT NULL,
  `title` varchar(64) NOT NULL,
  `url` varchar(128) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `app_startpic`
--

INSERT INTO `app_startpic` (`id`, `mid`, `appid`, `picurl`, `title`, `url`) VALUES
(8, 1, 4, 'http://localhost/weiduniang_app/Public/media/bgpic/21.png', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `app_styles`
--

CREATE TABLE IF NOT EXISTS `app_styles` (
  `cls` varchar(32) NOT NULL,
  `content` varchar(128) NOT NULL COMMENT '变量值',
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `appid` int(11) NOT NULL,
  `tpl` varchar(16) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `app_styles`
--

INSERT INTO `app_styles` (`cls`, `content`, `id`, `mid`, `appid`, `tpl`) VALUES
('background-img', '/weiduniang_app/Public/media/bgpic/01.png', 1, 1, 4, 'app1'),
('background-color', '#fff', 2, 1, 4, 'app1'),
('font-color', '', 3, 1, 4, 'app1'),
('title-color', '', 4, 1, 4, 'app1'),
('index-background-img', '', 5, 1, 4, 'app1'),
('index-background-color', '', 6, 1, 4, 'app1'),
('menu-background-color', '#7dc473', 7, 1, 4, 'app1'),
('menu-font-color', '#fff', 8, 1, 4, 'app1'),
('aside-background-color', '#000', 9, 1, 4, 'app1'),
('aside-font-color', '#0f0', 10, 1, 4, 'app1'),
('nav-background-color', '#00a650', 11, 1, 4, 'app1'),
('nav-font-color', '#fff', 12, 1, 4, 'app1'),
('title-font-color', '#fff', 13, 1, 4, 'app1'),
('title-background-color', '#00a650', 14, 1, 4, 'app1'),
('background-image', 'http://app.weiduniang.com/Public/media/bgpic/17.png', 15, 1, 4, 'app1'),
('index-background-image', '', 16, 1, 4, 'app1'),
('background-repeat', 'repeat', 17, 1, 4, 'app1'),
('background-position', 'center', 18, 1, 4, 'app1'),
('background-attachment', 'repeat', 19, 1, 4, 'app1'),
('body-opacity', '0.8', 20, 1, 4, 'app1');

-- --------------------------------------------------------

--
-- 表的结构 `app_user`
--

CREATE TABLE IF NOT EXISTS `app_user` (
`id` int(11) NOT NULL,
  `nick` varchar(16) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `username` varchar(16) NOT NULL,
  `age` mediumint(9) NOT NULL,
  `birthday` varchar(16) NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `addr` varchar(64) NOT NULL,
  `account` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL,
  `score` int(11) NOT NULL,
  `email` varchar(32) NOT NULL,
  `company` varchar(64) NOT NULL,
  `profession` varchar(16) NOT NULL,
  `position` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `app_widget`
--

CREATE TABLE IF NOT EXISTS `app_widget` (
`id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `dtype` varchar(12) NOT NULL,
  `url` varchar(128) NOT NULL,
  `attr` varchar(800) NOT NULL,
  `is_show` tinyint(4) NOT NULL,
  `is_menu` tinyint(4) NOT NULL,
  `is_page` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_app_info`
--
ALTER TABLE `app_app_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_info`
--
ALTER TABLE `app_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_magic_fileds`
--
ALTER TABLE `app_magic_fileds`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_magic_list`
--
ALTER TABLE `app_magic_list`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_magic_model`
--
ALTER TABLE `app_magic_model`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_magic_order`
--
ALTER TABLE `app_magic_order`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_member`
--
ALTER TABLE `app_member`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_nav`
--
ALTER TABLE `app_nav`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_pages`
--
ALTER TABLE `app_pages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_startpic`
--
ALTER TABLE `app_startpic`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_styles`
--
ALTER TABLE `app_styles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_user`
--
ALTER TABLE `app_user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_widget`
--
ALTER TABLE `app_widget`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_app_info`
--
ALTER TABLE `app_app_info`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `app_info`
--
ALTER TABLE `app_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `app_magic_fileds`
--
ALTER TABLE `app_magic_fileds`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_magic_list`
--
ALTER TABLE `app_magic_list`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_magic_model`
--
ALTER TABLE `app_magic_model`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_magic_order`
--
ALTER TABLE `app_magic_order`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_member`
--
ALTER TABLE `app_member`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_nav`
--
ALTER TABLE `app_nav`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `app_pages`
--
ALTER TABLE `app_pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `app_startpic`
--
ALTER TABLE `app_startpic`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `app_styles`
--
ALTER TABLE `app_styles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `app_user`
--
ALTER TABLE `app_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_widget`
--
ALTER TABLE `app_widget`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
