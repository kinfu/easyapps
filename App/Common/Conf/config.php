<?php
return array(
    'URL_MODEL'=>0,         // 如果你的环境不支持PATHINFO 请设置为3

    'DB_TYPE'=>'mysql',
    'DB_HOST'=>'',
    'DB_NAME'=>'',
    'DB_USER'=>'',
    'DB_PWD'=>'',
    'DB_PORT'=>'3306',
    'DB_PREFIX'=>'app_',



    'DB_DISCUZ_PREFIX' => 'pre_',

    'DEFAULT_MODULE' => 'Mobile',

    'MODULE_ALLOW_LIST'=>array('Admin','Weixin','Mobile','Rest'),


    'APP_NAME'=>'微度娘移动应用管理系统',

    'LOAD_EXT_CONFIG' => 'oauth', 
    'VAR_ADDON'    =>    'Widget'
);
?>
