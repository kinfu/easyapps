<?php
namespace Admin\Controller;
use Think\Controller;
class AppInfoController extends CommonController {

    public function index()
    {
        cookie( '_currentUrl_', __SELF__ );
        $this->display();
    }


    public function create()
    {
        $title = I('title');
        $intro = I('intro');
        $data['title'] = $title;
        $data['intro'] = $intro;
        $data['mid'] = $this->mid;
        $AppInfo = D('AppInfo');
        $appid = $AppInfo->add($data);
        redirect(U('Admin/Index/index?appid='.$appid));
    }

    public function update()
    {
        $AppInfo = D('AppInfo');

        if ( false === $AppInfo->create() ) {
          $this->error( $AppInfo->getError() );
        }
        
        $map['mid'] = $this->mid;
        $map['id'] = $this->appid;
        $list = $AppInfo->where( $map )->save();
        if ( false !== $list ) {
            redirect(cookie( '_currentUrl_' ));
        } else {
          //错误提示
          $this->error( '编辑失败!' );
        }
    }
}