<?php
namespace Admin\Controller;
use Think\Controller;
class CommonController extends Controller {
    public $group, $ac, $mod,$minfo;
    public $appid = 0,$appinfo;
    public $appUrl = '';
    public function _initialize()
    {
        
        $this->ac = ACTION_NAME;
        $this->mod = CONTROLLER_NAME;
        $this->group = GROUP_NAME;
        $this->assign('group', $this->group);
        $this->assign('mod', $this->mod);
        $this->assign('ac', $this->ac);
        $this->assign('thismod', $this->mod);
        $this->mid = session('mid');
        $this->assign('mid', $this->mid);

        
        if (!$this->mid) {
            $this->success('请先登录', U('Admin/Public/login'));
            exit();
        }

        //用户信息和权限
        $Member = D('Member');
        unset($map);
        $map['id'] = $this->mid;
        $this->minfo = $Member->where($map)->find();



        ///判断是否选择了应用
        if (I('appid')) session('appid',I('appid'));
        $this->appid = session('appid');
        if (!$this->appid && $this->mod !='AppInfo' 
            && $this->mod !='TableField' && $this->mod !='AdvTables' 
            && $this->ac !='chooseApp' ) {
            redirect(U('Admin/Index/chooseApp'));
            return;
        }
        unset($map);
        $AppInfo = D('AppInfo');
        $map['id'] = $this->appid;
        $this->appinfo = $AppInfo->getOne($map);
        $this->assign( 'appinfo_json', json_encode($this->appinfo));
        $this->assign( 'appinfo', $this->appinfo);

        $this->appUrl = "http://".$_SERVER['HTTP_HOST'].str_replace('admin.php', 'index.php', __APP__).'?appid='.$this->appid;
        $this->assign( 'appUrl', $this->appUrl);
        
    }


    public function add()
    {
        $this->display();
    }



    public function edit($id = 0) {
        $model = D( $this->mod );
        $map['id'] = $id;
        if (!$this->rid == 1) {
            $map['mid'] = $this->mid;
        }
        $vo = $model->getOne( $map );
        if ( !$vo ) {
            $this->error( "参数错误.非法参数." );
            exit();
        }
        $this->assign( 'vo', $vo );
        $this->display();
    }

    public function updateField($columnName ='',$id = 0,$value = '')
    {
        if ($columnName =="" || $id ==0) {
            echo($value);
            return;
        }
        $model = D($this->mod);
        $map['id'] = $id;
        $model->where($map)->setField($columnName,$value);
        $v = $model->where($map)->find();
        $key = 'table_filed_'.$v['mod'];
        S($key,NULL);
        echo($value);
    }

    public function insert()
    {
        $_POST['mid'] = $this->mid;
        $_POST['appid'] = $this->appid;
        $model = D( $this->mod );
        if ( false === $model->create() ) {
          $this->error( $model->getError() );
        }
        //保存当前数据对象
        $list = $model->add();
        if ( $list !== false ) { //保存成功
            // redirect(cookie( '_currentUrl_' ));
            // $this->success( '新增成功!', cookie( '_currentUrl_' ) );
        } else {
          //失败提示
          $this->error( '新增失败!' );
        }
    }

    function update($ajax = '') 
    {
        if ($ajax == 'updateField') {
            $columnName = I('columnName')?I('columnName'):$_GET['columnName'];
            $id = I('id')?I('id'):$_GET['id'];
            $value = I('value');
            if ($columnName =="" || $id ==0) {
                echo($value);
                return;
            }
            $model = D($this->mod);
            $map['id'] = $id;
            $model->where($map)->setField($columnName,$value);
            $v = $model->where($map)->find();
            $key = 'table_filed_'.$v['mod'];
            S($key,NULL);
            echo($value);
            return;
        }


        $model = D( $this->mod );
        if ( false === $model->create() ) {
          $this->error( $model->getError() );
        }
        $id = I( $model->getPk() );
        // 更新数据
        $map['mid'] = $this->mid;
        $map[$model->getPk()] = $id;
        $list = $model->where( $map )->save();
        if ( false !== $list ) {
          //成功提示
            afterNote('编辑成功');
            redirect(cookie( '_currentUrl_' ));
            // $this->success( '编辑成功!', cookie( '_currentUrl_' ) );
        } else {
          //错误提示
          $this->error( '编辑失败!' );
        }
    }

    public function getAuditMap()
    {
        if (!$this->rid == 1) {
            $map['mid'] = $this->mid;
        }
        return $map;
    }


    public function delete($id = 0,$ajax = 0)
    {
        $model = D( $this->mod );
        if ( !empty( $model ) ) {
          $pk = 'id';
          if ( isset( $id ) ) {
            $map[$pk] = array( 'in', explode( ',', $id ) );
            $restult = $model->where( $map )->delete();
            if ( false != $restult ) {
                $data['ret'] = 1;
                $data['msg'] =  "删除成功！";
                $this->ajaxReturn($data);
                // if (!$ajax) {
                //     afterNote('删除成功！');
                //     redirect(cookie( '_currentUrl_' ));
                // }
                // $this->success( '删除成功！' );
            } else {
              $this->error( '删除失败！' );
            }
          } else {
            $this->error( '非法操作' );
          }
        }
    }



    public function _list($model, $map, $sortBy = ' id desc')
    {
        $vlist = $model->getAll($map);
        $this->assign('volist',$vlist);
        return $vlist;
        $page = I('p',1);
        if ($page < 1) $page = 1;
        $pger  = $model->_list_($map,$page,16,$sortBy);
        $this->assign('volist',$pger['volist']);
        $this->assign('pger',$pger);

        $pgpam = $map;
        $pgvo =  array();
        unset($pgpam['mid']);
        for ($i = 0 ; $i < $pger['pagecount']; $i++) { 
            $pg = array();
            $pgpam['p'] = $i+1;
            $urlParems = http_build_query($pgpam);
            $pg['url'] = U($this->group.'/'.$this->mod.'/'.$this->ac.'?'.$urlParems);
            if ($page == $i+1) {
                $pg['url'] = "#";
                $pg['class'] = "active";
            }
            $pgvo[] = $pg;
        }
        $pgpam['p'] = $page+1;
        $urlParems = http_build_query($pgpam);
        $npage_url = U($this->group.'/'.$this->mod.'/'.$this->ac.'?'.$urlParems);
        $this->assign('npage_url',$npage_url);

        $pgpam['p'] = $page-1;
        $urlParems = http_build_query($pgpam);
        $ppage_url = U($this->group.'/'.$this->mod.'/'.$this->ac.'?'.$urlParems);
        $this->assign('ppage_url',$ppage_url);

        $this->assign('pgvo',$pgvo);
        $this->assign('npage',$page + 1);
        $this->assign('ppage',$page - 1);
        return $pger['volist'];
    }
}