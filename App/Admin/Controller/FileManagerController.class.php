<?php
namespace Admin\Controller;
use Think\Controller;
class FileManagerController extends CommonController {
	public function index()
	{
		$tmp_u = U('/','',false,true);
		$tmp_u = str_replace("admin.php?", "", $tmp_u);
		$tmp_u = str_replace("m=&", "", $tmp_u);
		$user_upload_dir = "Uploads/".numberDir($this->mid);
        $volist = array();
        $list = scandir($user_upload_dir);
        foreach($list as $file_name){
        	if ($file_name == "." || $file_name =="..") continue;
            $file_path = $user_upload_dir.$file_name;
            if (!file_exists($file_path)) continue;
            $vo = array();
            $vo['urldir'] = $tmp_u.$user_upload_dir;
            $vo['dir'] = $user_upload_dir;
            $vo['url'] = $tmp_u.$file_path;
            $vo['filepath'] = $file_path;
            $vo['filename'] = $file_name;
            $volist[] = $vo;
        }

        //在线素材 背景图片
        $user_upload_dir = 'Public/media/bgpic/';
        $onlist = array();
        $list = scandir($user_upload_dir);
        foreach($list as $file_name){
        	if ($file_name == "." || $file_name =="..") continue;
            $file_path = $user_upload_dir.$file_name;
            if (!file_exists($file_path)) continue;
            $vo = array();
            $vo['urldir'] = $tmp_u.$user_upload_dir;
            $vo['dir'] = $user_upload_dir;
            $vo['url'] = $tmp_u.$file_path;
            $vo['filepath'] = $file_path;
            $vo['filename'] = $file_name;
            $onlist[] = $vo;
        }

        //在线素材 图标
        $user_upload_dir = 'Public/media/bgpic/';
        $onlist = array();
        $list = scandir($user_upload_dir);
        foreach($list as $file_name){
            if ($file_name == "." || $file_name =="..") continue;
            $file_path = $user_upload_dir.$file_name;
            if (!file_exists($file_path)) continue;
            $vo = array();
            $vo['urldir'] = $tmp_u.$user_upload_dir;
            $vo['dir'] = $user_upload_dir;
            $vo['url'] = $tmp_u.$file_path;
            $vo['filepath'] = $file_path;
            $vo['filename'] = $file_name;
            $onlist[] = $vo;
        }


        //在线启动界面
        $user_upload_dir = 'Public/media/bgpic/';
        $onlist = array();
        $list = scandir($user_upload_dir);
        foreach($list as $file_name){
            if ($file_name == "." || $file_name =="..") continue;
            $file_path = $user_upload_dir.$file_name;
            if (!file_exists($file_path)) continue;
            $vo = array();
            $vo['urldir'] = $tmp_u.$user_upload_dir;
            $vo['dir'] = $user_upload_dir;
            $vo['url'] = $tmp_u.$file_path;
            $vo['filepath'] = $file_path;
            $vo['filename'] = $file_name;
            $onlist[] = $vo;
        }


        $this->assign('onlist',$onlist);
        $this->assign('volist',$volist);
		$this->display();
	}

	public function delete($fname='')
	{
		if (!$fname) return;
		$user_upload_dir = "Uploads/".numberDir($this->mid);
		$file_path = $user_upload_dir.$fname;
		unlink($file_path);
	}
}