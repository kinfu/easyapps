<?php
namespace Mobile\Controller;
use Think\Controller;
class IndexController extends CommonController {
    public function index()
	{        
        if (!$this->app['tpl']) return;
        $this->display($this->app['tpl'].'/index');
	}
}