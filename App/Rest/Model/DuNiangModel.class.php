<?php
namespace Rest\Model;
use Think\Model;
class DuNiangModel extends Model
{

    public $app = '';

    protected $connection = 'DB_WEIDUNIANG';

    protected $TableName = 'du_joke';

    public function _list_($map = array() ,$page = 1,$limit = 16, $orderby = ' id desc'){
        $list['count'] = $this->where($mmap)->count('id');
        $list['page'] = $page;
        $list['limit'] = $limit;
        $pagecount = ceil($list['count'] / $list['limit']);
        if ($pagecount < 1) $pagecount =1;
        $list['pagecount'] = $pagecount;
        $this->trueTableName = $this->TableName;
        $vlist = $this->where($map)->page($page,$limit)->order("$orderby ")->select();
        $vvlist = array();
        foreach ($vlist as $k) {
            if (method_exists($this, 'ckvo')) {
                $vvlist[] = $this->ckvo($k);
            }else{
                $vvlist[] = $k;
            }
        }
        $list['volist'] = $vvlist;
        return $list;
    }

    public function rand($limit = 6){
        $this->trueTableName = $this->TableName;
        $max = $this->count('id');
        $ids = $this->creatRand($max,$limit);
        $map['id'] = array('in',$ids);
        return  $this->_list_($map,$page,$limit);
    }

    //生成随机数 用于查询
    function creatRand($max = 10,$num = 1){
        $rand = '';
        for ($i=0; $i < $num; $i++) { 
            if ($rand) {
                $rand .=','; 
            }
            $rand .= rand(1,$max);
        }
        return $rand;
    }


}