<?php
namespace Rest\Controller;
use Think\Controller;
class NavController extends CommonController {
	public function index()
    {
    	$data['indexNavs'] = $this->getIndexNav();
    	$data['asides'] = $this->getAside();
    	$data['menus'] = $this->getMenu();
        $data['navs'] = $this->getNav();
    	$data['startPics'] = $this->getStartPics();
    	$this->ajaxReturn($data);
    }


    public function getIndexNav()
    {
        $s_key = 's_index_nav_'.$this->appid;
        if (S($s_key)) {
        	return S($s_key);
            return;
        }
        $Nav = D('Nav');
        $map['show_index'] = 1;
        $map['appid'] = $this->appid;
        $Indexnavs = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$Indexnavs);
        return $Indexnavs;
    }

    public function getAside()
    {
        $s_key = 's_aside_'.$this->appid;

        if (S($s_key)) {
            return S($s_key);
        }
        $map['pos'] = 'aside';
        $map['appid'] = $this->appid;
        $Nav = D('Nav');
        $asides = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$asides);
        return $asides;
    }

    public function getMenu()
    {
        $s_key = 's_menu_'.$this->appid;
        if (S($s_key)) {
        	return S($s_key);
            return;
        }
        $Nav = D('Nav');
        $map['pos'] = 'menu';
        $map['appid'] = $this->appid;
        $menus = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$menus);
        return $menus;
    }

    public function getNav()
    {
        $s_key = 's_nav_'.$this->appid;

        if (S($s_key)) {
        	return S($s_key);
            return;
        }
        $Nav = D('Nav');
        $map['pos'] = 'bottom';
        $map['appid'] = $this->appid;
        $navs = $Nav->getAll($map,'idx asc,id desc');
        S($s_key,$navs);
        return $navs;
    }
    //获取启动画面
    public function getStartPics()
    {
        $s_key = 's_startpic_'.$this->appid;
        if (S($s_key)) {
            return S($s_key);
        }
        $Startpic = D('Startpic');
        $map['appid'] = $this->appid;
        $startpics = $Startpic->getAll($map);
        S($s_key,$startpics);
        return $startpics;
    }
	public function getList($page = 1,$limit = 20,$navId = 0,$order = 'id desc',$format = 'json')
    {
    	if (!$navId) return;
		$Nav = D('Nav');

		$nav = $Nav->find($navId);

		$mod = parse_name($nav['module'],1);

		$model = D($mod);
		$model->app = $this->appinfo;
		if (!$model) return;

		$map['appid'] = $this->appid;
		$volist  = $model->_list_($nav,$map,$page,$limit);
		$volist['nav'] = $nav;
		$this->ajaxReturn($volist,$format);
    }


    public function view($navId = 0,$viewId = 0,$format = 'json')
    {
    	if (!$navId) return;
		$Nav = D('Nav');
		$nav = $Nav->find($navId);
		$mod = parse_name($nav['module'],1);
		$model = D($mod,'Service');
		$model->app = $this->appinfo;

		$map['appid'] = $this->appid ;
        $map['id'] = $viewId;
        $vo = $model->getOne($nav,$map);
        // print_r($vo);
        // $vo['nav'] = $nav;
        $this->ajaxReturn($vo,$format);
		
    }
}